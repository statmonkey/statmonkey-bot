import * as Discord from 'discord.js';
import {GuildChannel, GuildMember} from 'discord.js';
//Loading the properties from json file
import * as properties from '../resources/properties.json';
import {CommandHandler} from "./commands/CommandHandler";
import {GuildStatistic} from "./statistics/GuildStatistic";
//Only used for username changes
import {UserWrapper} from "./database/wrapper/UserWrapper";
import {GuildWrapper} from "./database/wrapper/GuildWrapper";
//Check user changes
import {BuildUserCache as buildUserCache} from './other/BuildUserCache'
import {CheckGuildDeletions as checkGuildDeletions} from "./other/CheckGuildDeletions";

//Initializing Discord Stuff
const client = new Discord.Client();
let commandHandler = new CommandHandler(properties.prefix);
let guildStatistics = new GuildStatistic();

let userWrapper = new UserWrapper();
//Only used for guild deletion
//Check user changes

let userCacheBuilt = false; //The actual ready event

//Ready event
client.on('ready', () => {
    client.user.setPresence({ game: { name: 'with numbers!' }, status: 'online' }).catch(console.error); //Setting the bot status
    buildUserCache(userWrapper, client).finally(() => userCacheBuilt = true);
    checkGuildDeletions(client);
});
//Ready event

//Message event
client.on('message', async (message) => {
    if (!userCacheBuilt) return;

    (await guildStatistics.getGuild(message.guild)).addMessage(message);
    commandHandler.execute(message);
});

client.on('messageDelete', async (message) => {
    if (!userCacheBuilt) return;

    (await guildStatistics.getGuild(message.guild)).removeMessage(message);
});

//Message event

client.on('channelCreate', async (guildChannel: GuildChannel) => {
    if (!userCacheBuilt || guildChannel.guild === null || guildChannel.guild === undefined || !guildStatistics.cachedGuilds.has(guildChannel.guild.id)) return;

    await (await guildStatistics.getGuild(guildChannel.guild)).createChannel(guildChannel);
});

client.on('channelDelete', async (guildChannel: GuildChannel) => {
    if (!userCacheBuilt || guildChannel.guild === null || guildChannel.guild === undefined || !guildStatistics.cachedGuilds.has(guildChannel.guild.id)) return;

    await (await guildStatistics.getGuild(guildChannel.guild)).removeChannel(guildChannel);
});

client.on('channelUpdate', async (oldGuildChannel: GuildChannel, newGuildChannel: GuildChannel) => {
    if (!userCacheBuilt || newGuildChannel.guild === null || newGuildChannel.guild === undefined || newGuildChannel.name === oldGuildChannel.name || !guildStatistics.cachedGuilds.has(newGuildChannel.guild.id)) return;

    await (await guildStatistics.getGuild(newGuildChannel.guild)).channelUpdate(oldGuildChannel, newGuildChannel);
});

client.on('guildDelete', async (guild) => {
    await new GuildWrapper(guild.id, undefined, undefined, undefined).deleteGuild();
    guildStatistics.cachedGuilds.delete(guild.id); //remove the guild from guildCache
});

client.on('guildMemberAdd', async (client: GuildMember) => {
    if (!userCacheBuilt || !guildStatistics.cachedGuilds.has(client.guild.id)) return;

    await (await guildStatistics.getGuild(client.guild)).registerUser(client);
});

client.on('guildMemberRemove', async (client: GuildMember) => {
    if (!userCacheBuilt || !guildStatistics.cachedGuilds.has(client.guild.id)) return;
    await (await guildStatistics.getGuild(client.guild)).removeUser(client);
});

client.on('voiceStateUpdate', async (oldUser, newUser) => {
    let oldId = oldUser.voiceChannelID;
    let newId = newUser.voiceChannelID;
    if (!userCacheBuilt || oldId === newId) return; //The user did not change channels

    (await guildStatistics.getGuild(newUser.guild)).voiceUpdate(oldUser, newUser);
});

client.on('userUpdate', async (oldClient, newClient) => {
    if (!userCacheBuilt || oldClient.username !== newClient.username) await userWrapper.changeUsername(newClient.id, newClient.username);
});

client.login(properties.token).catch(error => console.log("Error when logging in: " + error)); //Starting up the client
