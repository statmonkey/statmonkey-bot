import {Client} from "discord.js";
import {Saving} from "../database";

export function CheckGuildDeletions(discord: Client): void {
    Saving.pool.query('SELECT guild_id from guild').then(result => { //Get all the guilds inside the database
        result.rows.forEach(databaseGuild => { //Query the results one by one
            //Search for the discord guild inside database guild
            let found = false;
            discord.guilds.forEach(guild => {
                if (databaseGuild.guild_id === guild.id) {
                    found = true;
                }
            });
            //Search for the discord guild inside database guild

            //If the database guild is not found, set deleted
            if (!found) {
                Saving.pool.query('UPDATE guild SET deleted=TRUE WHERE guild_id=$1', [databaseGuild.guild_id]).then(() => {
                    console.log(`Deleted guild: ${databaseGuild.guild_id}`)
                }).catch(error => {
                    console.log(`Error deleting guild: ${databaseGuild.guild_id}\nError: ${error}`)
                })
            }
            //If the database guild is not found, set deleted
        });
    });
}