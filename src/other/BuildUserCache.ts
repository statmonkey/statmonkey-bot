import {UserWrapper} from "../database/wrapper/UserWrapper";
import {Client} from "discord.js";
import {Saving} from "../database";

export async function BuildUserCache(userWrapper: UserWrapper, client: Client): Promise<void> {
    userWrapper.getUsers().then(async () => { //Build the cache
        let databaseUsers = Array.from(Saving.userCache.values()); //Set the values of the users inside a cache

        for (let i = 0; i !== databaseUsers.length; i++) {
            let databaseUser = databaseUsers[i];
            client.fetchUser(databaseUser.userId.toString()).then(async discordUser => {
                if (discordUser.username !== databaseUser.username) await userWrapper.changeUsername(databaseUser.userId, discordUser.username); //If the database username is not the same as the guild username, update the database username
                else if (databaseUser.deleted) await userWrapper.updateDeleted(databaseUser.userId, false); //If the user is found, but marked as deleted on the database, mark as not deleted
            }).catch(() => userWrapper.updateDeleted(databaseUser.userId, true));
        }
    });
}
