import {RawGuildUserVoice} from "../database/model/RawModel";
import {Saving} from '../database';
import {
    Guild as DiscordGuild,
    GuildChannel as DiscordChannel,
    GuildMember,
    GuildMember as DiscordGuildMember,
    Message as DiscordMessage
} from 'discord.js'

import {registerGuild} from './utils/FirstJoined';
import {updateChanges} from "./utils/UpdateChanges";

export class GuildStatistic {
    readonly cachedGuilds: Map<string, GuildTasks>;

    constructor(){
        this.cachedGuilds = new Map();
    }

    async getGuild(guild: DiscordGuild): Promise<GuildTasks> {
        if (!this.cachedGuilds.has(guild.id)) {
            let guildTasks = new GuildTasks(guild);
            await guildTasks.init();
            this.cachedGuilds.set(guild.id, guildTasks);
        }
        return this.cachedGuilds.get(guild.id);
    }
}

class GuildTasks{
    readonly guild: DiscordGuild;
    readonly guildId: string;
    readonly guildName: string;
    readonly guildCreated: Date;
    readonly voiceChannelCache: Map<string, RawGuildUserVoice>;

    readonly saving: Saving;

    constructor(guild: DiscordGuild) {
        this.guild = guild;
        this.guildId = guild.id;
        this.guildName = guild.name;
        this.guildCreated = guild.createdAt;
        this.voiceChannelCache = new Map();

        this.saving = new Saving(this.guildId, this.guildName, this.guildCreated);
    }

    async init() {
        await this.saving.buildCache();

        if (!this.saving.guildWrapper.existed) {
            await registerGuild(this.guild, this.guildId, this.saving);
        } else {
            await updateChanges(this.guild, this.guildId, this.saving);
        }
    }

    async registerUser(guildMember: GuildMember): Promise<void> {
        await this.saving.guildUserWrapper.registerGuildUser(guildMember);
    }

    async removeUser(user: GuildMember): Promise<void> {
        await this.saving.guildUserWrapper.setLeftDate(user.id);
    }

    addMessage(message: DiscordMessage): void {
        if(message.id === undefined || message.author.id === undefined || message.channel.id === undefined || message.createdAt === undefined || message.channel.type === 'dm') return; //Don't log
        this.saving.guildUserMessageWrapper.addMessage(message.id, message.author.id, message.channel.id, message.createdAt, 0);
    }

    removeMessage(message: DiscordMessage): void {
        this.saving.guildUserMessageWrapper.removeMessage(message);
    }

    async createChannel(channel: DiscordChannel): Promise<void> {
        if (channel.type === 'text') {
            await this.saving.messageChannelWrapper.registerChannel(channel.id, channel.name, channel.createdAt);
        } else if (channel.type === 'voice') {
            await this.saving.voiceChannelWrapper.registerChannel(channel.id, channel.name, channel.createdAt);
        }
    }

    async removeChannel(channel: DiscordChannel): Promise<void> {
        if (channel.type == 'text') {
            await this.saving.messageChannelWrapper.deleteChannel(channel.id);
        } else if (channel.type == 'voice') {
            await this.saving.voiceChannelWrapper.deleteChannel(channel.id);
        }
    }

    async channelUpdate(oldChannel: DiscordChannel, newChannel: DiscordChannel): Promise<void> {
        if (newChannel.type == 'text') {
            await this.saving.messageChannelWrapper.updateChannelName(newChannel.id, newChannel.name);
        } else if (newChannel.type == 'voice') {
            await this.saving.voiceChannelWrapper.updateChannelName(newChannel.id, newChannel.name);
        }
    }

    voiceUpdate(oldState: DiscordGuildMember, newState: DiscordGuildMember): void {
        if (this.guild.afkChannel !== undefined && oldState.voiceChannel !== undefined && oldState.voiceChannel.id === this.guild.afkChannel.id) oldState = undefined;
        if (this.guild.afkChannel !== undefined && newState.voiceChannel !== undefined && newState.voiceChannel.id === this.guild.afkChannel.id) newState = undefined;

        if (oldState !== undefined) {
            let oldStateUser = this.voiceChannelCache.get(oldState.id);
            if (oldStateUser !== undefined) {
                this.saving.guildUserVoiceWrapper.addEntry(oldStateUser.userId, this.guildId, oldStateUser.channelId, oldStateUser.activeStart, new Date());
                this.voiceChannelCache.delete(oldStateUser.userId);
            }
        }

        if (newState !== undefined) {
            if (newState.voiceChannelID !== undefined) this.voiceChannelCache.set(newState.id, new RawGuildUserVoice(undefined, newState.voiceChannelID, this.guildId, newState.id, new Date(), null));
        }
    }
}