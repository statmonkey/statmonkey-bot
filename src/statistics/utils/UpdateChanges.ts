import {Guild, GuildChannel, GuildMember} from "discord.js";
import {Saving} from "../../database";

//On loading of guild, check for changes
export async function updateChanges(guild: Guild, guildId: string, saving: Saving): Promise<void> {

    //Check channel changes
    async function checkChannelChanges(): Promise<void> {

        //Check for channel name changes and for new channels
        async function checkChannelAddedAndNameChanges(): Promise<void> {
            let guildChannels = guild.channels.array(); //Build an array of all the discord guilds

            //Get the map of all the message channels and voice channels in cache
            let messageDatabaseChannels = saving.guildCache.messageChannel;
            let voiceDatabaseChannels = saving.guildCache.voiceChannel;
            //Get the map of all the message channels and voice channels in cache

            for (let guildChannelCounter = 0; guildChannelCounter !== guildChannels.length; guildChannelCounter++) { //Query all the discord guild channels
                let currentGuildChannel = guildChannels[guildChannelCounter]; //The current guild channel

                if (currentGuildChannel.type === 'voice') { //If the channel is a voice channel
                    let foundChannel = voiceDatabaseChannels.get(currentGuildChannel.id); //Search for channel inside database
                    if (foundChannel === undefined) { //New channel found, add it to the guild cache
                        await saving.voiceChannelWrapper.registerChannel(currentGuildChannel.id, currentGuildChannel.name, currentGuildChannel.createdAt);
                    } else if (foundChannel.channelName !== currentGuildChannel.name) {
                        await saving.voiceChannelWrapper.updateChannelName(foundChannel.channelId, currentGuildChannel.name); //Channel name change detected
                    }
                } else if (currentGuildChannel.type === 'text') { //If the channel is a voice channel
                    let foundChannel = messageDatabaseChannels.get(currentGuildChannel.id); //Search for channel inside database
                    if (foundChannel === undefined) { //New channel found, add it to the guild cache
                        await saving.messageChannelWrapper.registerChannel(currentGuildChannel.id, currentGuildChannel.name, currentGuildChannel.createdAt);
                        //New channel has been found! Add it
                    } else if (foundChannel.channelName !== currentGuildChannel.name) { //Channel name change detected
                        await saving.messageChannelWrapper.updateChannelName(foundChannel.channelId, currentGuildChannel.name);
                    }
                }
            }
        }

        //Check if channels have been removed
        async function checkChannelRemoved(): Promise<void> {
            let guildChannels = guild.channels.array(); //Load all the channels in an array

            //The message channels loaded in cache
            let cachedMessageChannels = saving.guildCache.messageChannel;
            let cachedMessageChannelKeys = Array.from(cachedMessageChannels.keys());
            //The message channels loaded in cache


            for (let i = 0; i !== cachedMessageChannelKeys.length; i++) { //Query through cached message channels
                let currentChannel = cachedMessageChannels.get(cachedMessageChannelKeys[i]); //The current channel to be queried
                if (currentChannel !== undefined && currentChannel.dateDeleted === undefined && searchForChannelGuild(currentChannel.channelId, guildChannels) === undefined) { //If the channel is defined and date deleted is undefined
                    await saving.voiceChannelWrapper.deleteChannel(currentChannel.channelId); //Delete the channel
                }
            }

            //The voice channels loaded in cache
            let cachedVoiceChannels = saving.guildCache.voiceChannel;
            let cachedVoiceChannelKeys = Array.from(cachedVoiceChannels.keys());
            //The voice channels loaded in cache

            for (let i = 0; i !== cachedVoiceChannelKeys.length; i++) { //Query through cached voice channels
                let currentChannel = cachedVoiceChannels.get(cachedVoiceChannelKeys[i]); //The current channel to be queried
                if (currentChannel !== undefined && currentChannel.dateDeleted === undefined && searchForChannelGuild(currentChannel.channelId, guildChannels) === undefined) { //If the channel is defined and date deleted is undefined
                    await saving.voiceChannelWrapper.deleteChannel(currentChannel.channelId); //Update deleted on database
                }
            }

        }

        await checkChannelAddedAndNameChanges();
        await checkChannelRemoved()
    }

    async function checkGuildChanges(): Promise<void> {

        async function checkGuildNameChange(): Promise<void> {
            if (guild.name !== saving.guildCache.guildName) { //If the guild's name is not the same, update the guild name
                let newName = guild.name;
                await saving.guildWrapper.updateGuildName(newName);
            }
        }

        //Checks for any user additions or user changes
        async function checkUsers(): Promise<void> {
            let guildWithUsers = await guild.fetchMembers(); //Load the guild with all the users (Just in case >250 users)
            let users = guildWithUsers.members.array(); //Load the guild users
            let guildUsers = saving.guildCache.guildUser; //Load all the database guild users

            for (let i = 0; i !== users.length; i++) { //Query through the guild members
                let member = users[i];
                let found = guildUsers.get(member.id);

                if (found === undefined) { //If the user is not found create it
                    await saving.guildUserWrapper.registerGuildUser(member);
                } else if (found.leftDate !== null || found.leftDate !== undefined) { //If the user is found and there is a left date, update the left date
                    await saving.guildUserWrapper.handleReJoin(member.user.id);
                }
            }

            //Check if a user has left
            await guildUsers.forEach(async guildUser => { //Query all the guildUsers in database
                let found = searchForGuildUser(guildUser.user.userId, users); //Search for the user inside of guild users

                if (found === undefined) {
                    await saving.guildUserWrapper.setLeftDate(guildUser.user.userId); //Remove user from database
                }
            });
        }

        await checkGuildNameChange();
        await checkUsers();
    }

    await checkChannelChanges();
    await checkGuildChanges();
}

function searchForChannelGuild(databaseChannelId: string, guildChannels: GuildChannel[]): GuildChannel {
    for (let i = 0; i != guildChannels.length; i++) {
        if (guildChannels[i].id === databaseChannelId) return guildChannels[i];
    }
    return undefined;
}

function searchForGuildUser(userId: string, guildUsersGuild: GuildMember[]): GuildMember {
    for (let i = 0; i != guildUsersGuild.length; i++) {
        if (guildUsersGuild[i].user.id === userId) return guildUsersGuild[i];
    }
    return undefined;
}
