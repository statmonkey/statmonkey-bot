import {Guild} from "discord.js";
import {Saving} from "../../database";
import {RawMessageChannel, RawVoiceChannel} from "../../database/model/RawModel";

//Load the newly joined guild to the database
export async function registerGuild(guild: Guild, guildId: string, saving: Saving): Promise<void> {
    async function registerChannels(): Promise<void> {
        let messageChannels: RawMessageChannel[] = [];
        let voiceChannels: RawVoiceChannel[] = [];

        for (let i = 0; i !== guild.channels.array().length; i++) {
            let channel = guild.channels.array()[i];

            //Build an array of RawMessageChannels and RawVoiceChannels
            if(channel.type === 'voice') {
                let voiceChannel = new RawVoiceChannel(channel.id, guildId, channel.name, channel.createdAt, undefined);
                voiceChannels.push(voiceChannel);
            } else if(channel.type === 'text') {
                let messageChannel = new RawMessageChannel(channel.id, guildId, channel.name, channel.createdAt, undefined);
                messageChannels.push(messageChannel);
            }
            //Build an array of RawMessageChannels and RawVoiceChannels
        }

        //Save the channels inside the database
        await saving.messageChannelWrapper.registerChannels(messageChannels);
        await saving.voiceChannelWrapper.registerChannels(voiceChannels);
        //Save the channels inside the database
    }

    //Register all the guild users
    let guildWithUsers = await guild.fetchMembers(); //Just in case guild has > 250 users
    await saving.guildUserWrapper.registerGuildUsers(guildWithUsers);
    //Register all the guild users

    await registerChannels(); //Registers all the channels
}
