import {UserWrapper} from "../database/wrapper/UserWrapper";
import {Message} from "discord.js";
import {FormattedMessage} from "./CommandHandler";

function main(message: Message, formattedMessage: FormattedMessage): void {
    let userWrapper = new UserWrapper();
    if(formattedMessage.parameters[0] === undefined || (formattedMessage.parameters[0].toLowerCase() !== 'on' && formattedMessage.parameters[0].toLowerCase() !== 'off')){
        message.reply("Expected parameter: On/Off");
        return;
    }

    let status = (formattedMessage.parameters[0].toLowerCase() === 'on');

    userWrapper.updatePrivate(message.author.id, status);
    message.reply("Private Account has been turned " + formattedMessage.parameters[0].toLowerCase());
}

export {main};