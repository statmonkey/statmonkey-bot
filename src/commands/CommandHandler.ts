//Importing HashMap module
import {Message} from "discord.js";
import * as LoadedCommands from '../../resources/commands.json'

//Setting each command
class Command{
    readonly command: string;
    readonly description: string;
    readonly run: any;

    //Constructor to set the path, command and desc.
    constructor(fileName: string, command: string, description: string) {
        this.command = command;
        this.description = description;
        this.run = require(`./${fileName}`).main;
    }
}

//Handling commands
class CommandHandler{
    readonly prefix: string;
    readonly commands: Map<string, Command>;
    readonly prefixRegexExpression: RegExp;

    constructor(prefix: string) {
        this.prefix = prefix;

        this.commands = new Map();
        this.prefixRegexExpression = new RegExp("^(["+prefix+"])"); //RegExp prefix

        this.loadCommands();
    }

    //Loads the command in the Command class
    loadCommands(): void {
        let loadedCommands = LoadedCommands.list;

        for (let index = 0; index < loadedCommands.length; index++){
            let command = new Command(loadedCommands[index].filename, loadedCommands[index].command, loadedCommands[index].description);
            this.commands.set(loadedCommands[index].command, command);
        }
    }

    //Extracts the command by searching for it using recursions
    extractCommand(currentlyApplicable: string[], command: string[], currentLocation: number): string | undefined {
        if (command.length + 1 === currentLocation) return undefined; //Command not found

        if (currentLocation === 0) currentlyApplicable = Array.from(this.commands.keys());

        let newApplicable = [];
        for (let i = 0; i !== currentlyApplicable.length ; i++) {
            let currentCommand = currentlyApplicable[i].split(" ");
            if(command.length >= currentCommand.length && currentCommand[currentLocation] === command[currentLocation]){
                newApplicable.push(currentCommand.join(" "));
            }
        }

        if (newApplicable.length === 1) return newApplicable[0];
        else this.extractCommand(newApplicable, command, ++currentLocation);
    }

    //Checking and formatting the message
    formatMessage(message: Message): FormattedMessage {
        if (message.author.bot || !this.prefixRegexExpression.exec(message.content)) return; //If the author is a bot or if it does not match the prefix just cancel

        //Splitting the text into an array
        let content = message.content.split(" ");
        content[0] = content[0].substr(this.prefix.length);

        //Checks if the command exists and returns the command
        let command = this.extractCommand(undefined, content, 0);
        if(command === undefined) return;
        //Checks if the command exists and returns the command

        //Setting the parameters and other stuff
        let parameters = content;
        let splitCommand = command.toString().split(" ");

        for (let i = 0; i !== splitCommand.length ; i++) {
            parameters.shift();
        }
        //Setting the parameters and other stuff

        return new FormattedMessage(command, parameters);
    }

    //Runs the command
    executeCommand(message: Message, formattedMessage: FormattedMessage): void {
        if(formattedMessage === undefined || !this.commands.has(formattedMessage.command)) return; //Command does not exist
        this.commands.get(formattedMessage.command).run(message, formattedMessage);
    }

    //Calls the executeCommand with all the needed information
    execute(message: Message): void {
        this.executeCommand(message, this.formatMessage(message));
    }
}

class FormattedMessage {
    readonly command: string;
    readonly parameters: string[];


    constructor(command: string, parameters: string[]) {
        this.command = command;
        this.parameters = parameters;
    }
}

export {CommandHandler, FormattedMessage};