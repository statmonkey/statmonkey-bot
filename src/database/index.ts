import {Pool} from "pg";
import * as properties from '../../resources/properties.json';

import {GuildWrapper} from "./wrapper/GuildWrapper";
import {UserWrapper} from "./wrapper/UserWrapper";

import {MessageChannelWrapper} from "./wrapper/MessageChannelWrapper";
import {VoiceChannelWrapper} from "./wrapper/VoiceChannelWrapper";

import {GuildUserWrapper} from "./wrapper/GuildUserWrapper";
import {GuildUserVoiceWrapper} from "./wrapper/GuildUserVoiceWrapper";
import {GuildUserMessageWrapper} from "./wrapper/GuildUserMessageWrapper";

import {Guild, User} from './model/LinkedModel';

export class Saving {
    static pool: Pool;
    static userCache: Map<string, User>;

    readonly guildId: string;
    readonly guildName: string;
    readonly guildCreated: Date;

    guildWrapper: GuildWrapper;
    userWrapper: UserWrapper;

    messageChannelWrapper: MessageChannelWrapper;
    voiceChannelWrapper: VoiceChannelWrapper;

    guildUserWrapper: GuildUserWrapper;
    guildUserVoiceWrapper: GuildUserVoiceWrapper;
    guildUserMessageWrapper: GuildUserMessageWrapper;

    guildCache: Guild;

    constructor(guildId: string, guildName: string, guildCreated: Date) {
        this.guildId = guildId;
        this.guildName = guildName;
        this.guildCreated = guildCreated;
    }

    async buildCache(): Promise<void> {
        this.guildWrapper = new GuildWrapper(this.guildId, this.guildName, this.guildCreated, undefined);

        this.guildCache = Guild.convertToLinked(await this.guildWrapper.getGuild());
        this.guildWrapper.cache = this.guildCache;

        this.userWrapper = new UserWrapper();
        this.messageChannelWrapper = new MessageChannelWrapper(this.guildId, this.guildCache);
        this.voiceChannelWrapper = new VoiceChannelWrapper(this.guildId, this.guildCache);
        this.guildUserWrapper = new GuildUserWrapper(this.guildId, this.guildCache);
        this.guildUserVoiceWrapper = new GuildUserVoiceWrapper(this.guildId, this.guildCache);
        this.guildUserMessageWrapper = new GuildUserMessageWrapper(this.guildId, this.guildCache);

        await this.messageChannelWrapper.getAllChannels();
        await this.voiceChannelWrapper.getAllChannels();
        await this.guildUserWrapper.getGuildUsers();
    }

    static initPool(): void {
        this.pool = new Pool({connectionString: properties.sqlConnection});
        this.pool.on('connect', async (client) => await client.query(`SET search_path TO ${properties.schema}`))
    }

    static initUserCache(): void {
        this.userCache = new Map<string, User>();
    }
}

Saving.initUserCache();
Saving.initPool();

