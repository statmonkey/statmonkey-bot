class RawUser {
    readonly userId: string;
    readonly bot: boolean;
    readonly dateJoined: Date;

    username: string;
    deleted: boolean;
    privateAccount: boolean;

    constructor(userId: string, bot: boolean, username: string, deleted: boolean = false, privateAccount: boolean = false, dateJoined: Date) {
        this.userId = userId;
        this.bot = bot;
        this.username = username;
        this.deleted = deleted;
        this.privateAccount = privateAccount;
        this.dateJoined = dateJoined;
    }

    static buildDatabaseMapping(jsonObject: any): RawUser {
        return new RawUser(jsonObject.user_id, jsonObject.is_bot, jsonObject.username, jsonObject.deleted, jsonObject.private, jsonObject.date_joined);
    }
}

class RawGuild {
    readonly guildId: string;
    guildName: string;

    readonly guildCreated: Date;
    readonly guildJoined: Date;

    deleted: boolean;

    constructor(guildId: string, guildName: string, guildCreated: Date, guildJoined: Date, deleted: boolean = false) {
        this.guildId = guildId;
        this.guildName = guildName;
        this.guildCreated = guildCreated;
        this.guildJoined = guildJoined;
        this.deleted = deleted;
    }

    static buildDatabaseMapping(jsonObject: any): RawGuild {
        return new RawGuild(jsonObject.guild_id, jsonObject.guild_name, jsonObject.guild_created, jsonObject.guild_joined, jsonObject.deleted);
    }
}

class RawGuildUser {
    readonly guildId: string;
    readonly userId: string;
    readonly joinedDate: Date;
    leftDate: Date;

    constructor(guildId: string, userId: string, joinedDate: Date, leftDate: Date | undefined) {
        this.guildId = guildId;
        this.userId = userId;
        this.joinedDate = joinedDate;
        this.leftDate = leftDate;
    }

    static buildDatabaseMapping(jsonObject: any): RawGuildUser {
        return new RawGuildUser(jsonObject.guild_id, jsonObject.user_id, jsonObject.joined_date, jsonObject.left_date);
    }
}

class RawVoiceChannel {
    readonly channelId: string;
    readonly guildId: string;
    readonly dateCreated: Date;
    readonly dateDeleted: Date | undefined;
    channelName: string;

    constructor(channelId: string, guildId: string, channelName: string, dateCreated: Date, dateDeleted: Date | undefined) {
        this.channelId = channelId;
        this.guildId = guildId;
        this.dateCreated = dateCreated;
        this.dateDeleted = dateDeleted;
        this.channelName = channelName;
    }

    static buildDatabaseMapping(jsonObject: any): RawVoiceChannel {
        return new RawVoiceChannel(jsonObject.voice_channel_id, jsonObject.guild_id, jsonObject.channel_name, jsonObject.date_created, jsonObject.date_deleted);
    }
}

class RawMessageChannel {
    readonly channelId: string;
    readonly guildId: string;

    channelName: string;
    dateCreated: Date;
    dateDeleted: Date | undefined;

    constructor(channelId: string, guildId: string, channelName: string, dateCreated: Date, dateDeleted: Date | undefined) {
        this.channelId = channelId;
        this.guildId = guildId;
        this.dateCreated = dateCreated;
        this.dateDeleted = dateDeleted;
        this.channelName = channelName;
    }

    static buildDatabaseMapping(jsonObject: any): RawMessageChannel {
        return new RawMessageChannel(jsonObject.message_channel_id, jsonObject.guild_id, jsonObject.channel_name, jsonObject.date_created, jsonObject.date_deleted);
    }
}

class RawGuildUserVoice {
    readonly id: string;

    readonly guildId: string;
    readonly userId: string;
    readonly channelId: string;

    readonly activeStart: Date;
    readonly activeEnd: Date;

    constructor(id: string, channelId: string, guildId: string, userId: string, activeStart: Date, activeEnd: Date) {
        this.id = id;

        this.guildId = guildId;
        this.userId = userId;
        this.channelId = channelId;

        this.activeStart = activeStart;
        this.activeEnd = activeEnd;
    }

    static buildDatabaseMapping(jsonObject: any): RawGuildUserVoice {
        return new RawGuildUserVoice(jsonObject.id, jsonObject.voice_channel_id, jsonObject.guild_id, jsonObject.user_id, jsonObject.active_start, jsonObject.active_end);
    }
}

class RawGuildUserMessage {
    readonly messageId: string;

    readonly guildId: string;
    readonly userId: string;
    readonly channelId: string;

    readonly typeOfMessage: number;
    readonly timeSent: Date;
    readonly timeDeleted: Date;

    constructor(messageId: string, guildId: string, userId: string, channelId: string, typeOfMessage: number, timeSent: Date, timeDeleted: Date) {
        this.messageId = messageId;

        this.guildId = guildId;
        this.userId = userId;
        this.channelId = channelId;

        this.typeOfMessage = typeOfMessage;
        this.timeSent = timeSent;
        this.timeDeleted = timeDeleted;
    }

    static buildDatabaseMapping(jsonObject: any): RawGuildUserMessage {
        return new RawGuildUserMessage(jsonObject.message_id, jsonObject.guild_id, jsonObject.user_id, jsonObject.message_channel_id, jsonObject.type_of_message, jsonObject.time_sent, jsonObject.time_deleted);
    }
}

export {RawUser, RawGuild, RawGuildUser, RawVoiceChannel, RawMessageChannel, RawGuildUserVoice, RawGuildUserMessage};