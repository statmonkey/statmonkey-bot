import {RawGuild, RawGuildUser, RawMessageChannel, RawUser, RawVoiceChannel} from "./RawModel";

class Guild{
    readonly guildId: string;
    guildName: string;

    readonly guildCreated: Date;
    readonly guildJoined: Date;

    deleted: boolean;

    guildUser: Map<string, GuildUser>;
    voiceChannel: Map<string, VoiceChannel>;
    messageChannel: Map<string, MessageChannel>;

    constructor(guildId: string, guildName: string, guildCreated: Date, guildJoined: Date, deleted: boolean) {
        this.guildId = guildId;
        this.guildName = guildName;
        this.guildCreated = guildCreated;
        this.guildJoined = guildJoined;
        this.deleted = deleted;

        this.guildUser = new Map<string, GuildUser>();

        this.voiceChannel = new Map<string, VoiceChannel>();
        this.messageChannel = new Map<string, MessageChannel>();
    }

    static convertToLinked(unlinkedClass: RawGuild): Guild {
        return new Guild(unlinkedClass.guildId, unlinkedClass.guildName, unlinkedClass.guildCreated, unlinkedClass.guildJoined, unlinkedClass.deleted);
    }
}

class User{
    readonly userId: string;
    readonly bot: boolean;
    readonly dateJoined: Date;

    username: string;
    deleted: boolean;
    privateAccount: boolean;

    guildUser: Map<string, GuildUser>;

    constructor(userId: string, bot: boolean, username: string, deleted: boolean = false, privateAccount: boolean, dateJoined: Date) {
        this.userId = userId;
        this.bot = bot;
        this.username = username;
        this.deleted = deleted;
        this.privateAccount = privateAccount;
        this.dateJoined = dateJoined;

        this.guildUser = new Map<string, GuildUser>();
    }


    static convertToLinked(unlinkedClass: RawUser): User {
        return new User(unlinkedClass.userId, unlinkedClass.bot, unlinkedClass.username, unlinkedClass.deleted, unlinkedClass.privateAccount, unlinkedClass.dateJoined);
    }
}

class GuildUser{
    readonly guild: Guild;
    readonly user: User;
    readonly joinedDate: Date;
    leftDate: Date | undefined;

    constructor(guild: Guild, user: User, joinedDate: Date, leftDate: Date | undefined) {
        this.guild = guild;
        this.user = user;
        this.joinedDate = joinedDate;
        this.leftDate = leftDate;
    }

    static convertToLinked(guild: Guild, user: User, unlinkedClass: RawGuildUser): GuildUser {
        return new GuildUser(guild, user, unlinkedClass.joinedDate, unlinkedClass.leftDate);
    }
}

class VoiceChannel{
    readonly channelId: string;
    channelName: string;
    readonly guild: Guild;
    readonly dateCreated: Date;
    dateDeleted: Date | undefined;

    constructor(channelId: string, channelName: string, guild: Guild, dateCreated: Date, dateDeleted: Date | undefined) {
        this.channelId = channelId;
        this.guild = guild;
        this.dateCreated = dateCreated;
        this.dateDeleted = dateDeleted;
        this.channelName = channelName;
    }

    static convertToLinked(guild: Guild, unlinkedClass: RawVoiceChannel): VoiceChannel {
        return new VoiceChannel(unlinkedClass.channelId, unlinkedClass.channelName, guild, unlinkedClass.dateCreated, unlinkedClass.dateDeleted);
    }
}

class MessageChannel{
    readonly channelId: string;
    channelName: string;
    readonly guild: Guild;
    readonly dateCreated: Date;
    dateDeleted: Date | undefined;

    constructor(channelId: string, channelName: string, guild: Guild, dateCreated: Date, dateDeleted: Date | undefined) {
        this.channelId = channelId;
        this.guild = guild;
        this.dateCreated = dateCreated;
        this.dateDeleted = dateDeleted;
        this.channelName = channelName;
    }

    static convertToLinked(guild: Guild, unlinkedClass: RawMessageChannel): MessageChannel {
        return new MessageChannel(unlinkedClass.channelId, unlinkedClass.channelName, guild, unlinkedClass.dateCreated, unlinkedClass.dateDeleted);
    }
}

export {User, Guild, GuildUser, VoiceChannel, MessageChannel};
