import {Base} from './Base';
import {Message} from 'discord.js'
import {Guild} from "../model/LinkedModel";

export class GuildUserMessageWrapper extends Base {
    table: string;

    constructor(guildId: string, cache: Guild) {
        super(guildId, cache);
        this.table = "guild_user_message";
    }

    addMessage(messageId: string, userId: string, channelId: string, timeSent: Date, typeOfMessage: number): void {
        let query = `INSERT INTO guild_user_message(message_id, user_id, guild_id, message_channel_id, type_of_message,
                                                    time_sent)
                     VALUES ($1, $2, $3, $4, $5, $6);`;
        let values: any[] = [messageId, userId, this.guildId, channelId, typeOfMessage, timeSent];

        this.doTransaction(query, values).catch(error => Base.errorHandler(query, error));
    }

    removeMessage(message: Message): void {
        let query = `UPDATE guild_user_message
                     SET time_deleted=$1
                     WHERE message_id = $2`;

        this.doTransaction(query, [new Date(), message.id]).catch(async () => {
            await this.addMessage(message.id, message.author.id, message.channel.id, message.createdAt, 0);
            this.removeMessage(message);
        });
    }
}
