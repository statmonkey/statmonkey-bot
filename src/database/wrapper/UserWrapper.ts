import {RawUser} from "../model/RawModel";
import {Base} from './Base';
import {User} from "../model/LinkedModel";
import {EventEmitter} from "events";
import {Saving} from "../index";

export class UserWrapper extends Base {
    readonly table: string;
    static eventEmitter: EventEmitter;

    constructor() {
        super(undefined, undefined);
        this.table = "user";
    }

    async getUsers(): Promise<void> {
        let query = `SELECT *
                     FROM "user"`;

        try {
            let users = await super.doTransaction(query);

            for (let i = 0; i !== users.rows.length; i++) {
                let currentUser = users.rows[i];

                let linkedUser = User.convertToLinked(RawUser.buildDatabaseMapping(currentUser));
                Saving.userCache.set(linkedUser.userId, linkedUser);
            }
        } catch (e) {
            Base.errorHandler(query, e);
        }
    }

    registerUser(userId: string, username: string, isBot: boolean): void {
        let query = 'INSERT INTO "user"(user_id, is_bot, username, date_joined) VALUES ($1, $2, $3, $4);';
        let values = [userId, username, isBot, new Date()];

        super.doTransaction(query, values).catch(error => Base.errorHandler(query, error));
    }

    registerUsers(users: RawUser[]): void {
        this.session.connect().then(async (client) => {
            try {
                let userSearchQuery = 'SELECT * FROM "user" WHERE user_id=$1';
                let userAddQuery = 'INSERT INTO "user"(user_id, is_bot, username, date_joined) VALUES ($1, $2, $3, $4)';
                for (let i = 0; i !== users.length; i++) {
                    let currentUser = users[i];

                    //Check if the user is registered
                    let foundUser = await client.query(userSearchQuery, [currentUser.userId]);
                    if (foundUser.rows === undefined || foundUser.rowCount === 0) {
                        await client.query(userAddQuery, [currentUser.userId, currentUser.bot, currentUser.username, new Date()]);
                    }
                    //Check if the user is registered
                }
            } catch (e) {
                Base.errorHandler("Register Users: ", e);
            } finally {
                client.release();
            }
        });
    }

    async changeUsername(userId: string, newUsername: string): Promise<void> {
        let query = 'UPDATE "user" SET username=$1 WHERE user_id=$2;';
        let values = [newUsername, userId];

        await super.doTransaction(query, values).catch(error => Base.errorHandler(query, error));
        Saving.userCache.get(userId).username = newUsername;
    }

    async updateDeleted(userId: string, newValue: boolean): Promise<void> {
        let query = 'UPDATE "user" SET deleted=$1 WHERE user_id=$2;';
        let values = [newValue, userId];

        await super.doTransaction(query, values).catch(error => Base.errorHandler(query, error));
        Saving.userCache.get(userId).deleted = newValue;
    }

    async updatePrivate(userId: string, newValue: boolean): Promise<void> {
        let query = 'UPDATE "user" SET private=$1 WHERE user_id=$2;';
        let values = [newValue, userId];

        await super.doTransaction(query, values).catch(error => Base.errorHandler(query, error));
        Saving.userCache.get(userId).privateAccount = newValue;
    }
}

UserWrapper.eventEmitter = new EventEmitter();
