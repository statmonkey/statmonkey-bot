import {RawGuildUser} from '../model/RawModel';
import {Guild, GuildUser, User} from '../model/LinkedModel';
import {Base} from './Base';
import {Guild as DiscordGuild, GuildMember} from 'discord.js';
import {Saving} from "../index";

export class GuildUserWrapper extends Base {
    readonly table: string;
    userCache: Map<string, User>;

    constructor(guildId: string, cache: Guild) {
        super(guildId, cache);
        this.table = "guild_user";
        this.userCache = Saving.userCache;
    }

    /*
    //to be checked
    getGuildUser(userId: string): Promise<RawGuildUser | undefined> {
        let searchQuery = "SELECT * FROM guild_user WHERE guild_id=$1 AND user_id=$2";

        return super.doTransaction(searchQuery, [userId, this.guildId]).then(result => {
            if (result.rowCount === 0) return undefined;
            else return RawGuildUser.buildDatabaseMapping(result);
        }).catch(error => {
            Base.errorHandler(searchQuery, error);
            return undefined;
        });
    }
     */

    async getGuildUsers(): Promise<GuildUser[] | undefined> {
        const client = await this.session.connect();
        try {
            let guildUsers = await super.exists(this.table, "guild_id", this.guildId);
            if (!guildUsers.exists) return undefined;

            let rows = guildUsers.rows;
            let guildUserMapping: GuildUser[] = [];

            for (let i = 0; i !== rows.length; i++) {
                let currentGuildUser = RawGuildUser.buildDatabaseMapping(rows[i]);
                let user = this.userCache.get(currentGuildUser.userId);

                if (user.deleted) {
                    await client.query('UPDATE "user" SET deleted=$1 WHERE user_id=$2;', [false, currentGuildUser.userId]);
                    user.deleted = false;
                }

                let guildUser = new GuildUser(this.cache, user, currentGuildUser.joinedDate, currentGuildUser.leftDate);
                user.guildUser.set(currentGuildUser.guildId, guildUser);

                this.cache.guildUser.set(guildUser.user.userId, guildUser);
            }
            return guildUserMapping;
        } catch (error) {
            Base.errorHandler("Getting all guild users", error);
        } finally {
            client.release();
        }
    }

    async registerGuildUser(guildMember: GuildMember): Promise<void> {
        let userAddQuery = 'INSERT INTO "user"(user_id, is_bot, username, date_joined) VALUES ($1, $2, $3, $4)';

        let guildUserSearchQuery = "SELECT * FROM guild_user WHERE guild_id=$1 AND user_id=$2";
        let guildUserAddQuery = 'INSERT INTO guild_user(guild_id, user_id, joined_date) VALUES ($1, $2, $3);';

        let rejoinQuery = 'UPDATE guild_user SET left_date=$1 WHERE guild_id=$2 AND user_id=$3;';

        const client = await this.session.connect();
        try {
            //Search if the user is initially registered, if not register him
            let foundUser = this.userCache.get(guildMember.user.id);
            if (foundUser === undefined) {
                await client.query(userAddQuery, [guildMember.user.id, guildMember.user.bot, guildMember.user.username, guildMember.user.createdAt]);
                this.userCache.set(guildMember.user.id, new User(guildMember.user.id, guildMember.user.bot, guildMember.user.username, false, false, guildMember.user.createdAt))
            } else if (foundUser.deleted) {
                await client.query('UPDATE "user" SET deleted=$1 WHERE user_id=$2;', [false, foundUser.userId]);
                foundUser.deleted = false;
            }
            //Search if the user is initially registered, if not register him

            //If the user is not found in guildUsers, add the user
            let foundGuildUser = await client.query(guildUserSearchQuery, [this.guildId, guildMember.user.id]);
            if (foundGuildUser.rows === undefined || foundGuildUser.rowCount === 0) {
                await client.query(guildUserAddQuery, [this.guildId, guildMember.user.id, guildMember.joinedAt]);
                let guildUser = new GuildUser(this.cache, foundUser, guildMember.joinedAt, undefined);
                foundUser.guildUser.set(this.guildId, guildUser);

                this.cache.guildUser.set(guildUser.user.userId, guildUser);
            } else if (foundGuildUser.rows[0].left_date !== null || foundGuildUser.rows[0].left_date !== undefined) {
                //If the user had previously left, remove left date
                await client.query(rejoinQuery, [null, this.guildId, guildMember.user.id]);
                this.cache.guildUser.get(guildMember.user.id).leftDate = undefined;
            }

            //If the user is not found in guildUsers, add the user
        } catch (e) {
            GuildUserWrapper.errorHandler("Error registering user " + guildMember.user.id, e)
        } finally {
            client.release();
        }
    }

    async registerGuildUsers(guild: DiscordGuild): Promise<void> {
        let userAddQuery = 'INSERT INTO "user"(user_id, is_bot, username, date_joined) VALUES ($1, $2, $3, $4)';

        let guildUserSearchQuery = "SELECT * FROM guild_user WHERE guild_id=$1 AND user_id=$2";
        let guildUserAddQuery = 'INSERT INTO guild_user(guild_id, user_id, joined_date) VALUES ($1, $2, $3);';

        let rejoinQuery = 'UPDATE guild_user SET left_date=$1 WHERE guild_id=$2 AND user_id=$3;';

        const client = await this.session.connect();
        try {
            for (let i = 0; i !== guild.members.array().length; i++) {
                let currentMember = guild.members.array()[i];

                //Search if the user is initially registered, if not register him
                let foundUser = this.userCache.get(currentMember.user.id);
                if (foundUser === undefined) {
                    await client.query(userAddQuery, [currentMember.user.id, currentMember.user.bot, currentMember.user.username, currentMember.user.createdAt]);
                    this.userCache.set(currentMember.user.id, new User(currentMember.user.id, currentMember.user.bot, currentMember.user.username, false, false, currentMember.user.createdAt))
                } else if (foundUser.deleted) {
                    await client.query('UPDATE "user" SET deleted=$1 WHERE user_id=$2;', [false, foundUser.userId]);
                    foundUser.deleted = false;
                }
                //Search if the user is initially registered, if not register him

                //If the user is not found in guildUsers, add the user
                let foundGuildUser = await client.query(guildUserSearchQuery, [currentMember.user.id, this.guildId]);
                if (foundGuildUser.rows === undefined || foundGuildUser.rowCount === 0) {
                    await client.query(guildUserAddQuery, [this.guildId, currentMember.user.id, currentMember.joinedAt]);
                    let guildUser = new GuildUser(this.cache, foundUser, currentMember.joinedAt, undefined);
                    foundUser.guildUser.set(this.guildId, guildUser);

                    this.cache.guildUser.set(guildUser.user.userId, guildUser);
                } else if (foundGuildUser.rows[0].left_date !== null || foundGuildUser.rows[0].left_date !== undefined) {
                    await client.query(rejoinQuery, [null, this.guildId, currentMember.user.id]);
                    this.cache.guildUser.get(currentMember.user.id).leftDate = undefined;
                }
                //If the user is not found in guildUsers, add the user
            }
        } catch (e) {
            GuildUserWrapper.errorHandler("Error registering users", e);
        } finally {
            client.release();
        }
    }

    async setLeftDate(userId: string): Promise<void> {
        let query = 'UPDATE guild_user SET left_date=$1 WHERE guild_id=$2 AND user_id=$3;';
        let currentDate = new Date();
        let values = [currentDate, this.guildId, userId];

        await super.doTransaction(query, values).catch(error => Base.errorHandler(query, error));
        this.cache.guildUser.get(userId).leftDate = currentDate;
    }

    async handleReJoin(userId: string): Promise<void> {
        let query = 'UPDATE guild_user SET left_date=$1 WHERE guild_id=$2 AND user_id=$3;';
        let values = [null, this.guildId, userId];

        await super.doTransaction(query, values).catch(error => Base.errorHandler(query, error));
        this.cache.guildUser.get(userId).leftDate = undefined;
    }
}
