import {RawGuild} from '../model/RawModel';
import {Base} from './Base';
import {Guild} from "../model/LinkedModel";

export class GuildWrapper extends Base {
    readonly table: string;
    readonly guildCreated: Date;
    readonly guildName: string;

    existed: boolean;

    constructor(guildId: string, guildName: string, guildCreated: Date, cache: Guild) {
        super(guildId, cache);
        this.guildName = guildName;
        this.guildCreated = guildCreated;
        this.table = "guild";
    }

    async getGuild(): Promise<RawGuild> {
        try {
            let result = await super.exists(this.table, "guild_id", this.guildId);
            if (result.exists === true) {
                this.existed = true;

                if (result.rows[0].deleted === true) {
                    await this.reactivateGuild();
                    result.rows[0].deleted = false;
                }

                return (RawGuild.buildDatabaseMapping(result.rows[0]));
            } else {
                this.existed = false;
                await this.createGuild();
                return (RawGuild.buildDatabaseMapping({
                    "guild_id": this.guildId,
                    "guild_name": this.guildName,
                    "guild_created": this.guildCreated,
                    "guild_joined": new Date(),
                    "deleted": false
                }));
            }
        } catch (e) {
            Base.errorHandler(undefined, e)
        }
    }

    async createGuild(): Promise<void> {
        let query = 'INSERT INTO guild(guild_id, guild_name, created_date, guild_joined) VALUES ($1, $2, $3, $4);';
        let values = [this.guildId, this.guildName, this.guildCreated, new Date()];

        await super.doTransaction(query, values).catch(error => Base.errorHandler(query, error));
    }

    async updateGuildName(name: string): Promise<void> {
        let query = 'UPDATE guild SET guild_name=$1 WHERE guild_id=$2;';
        let values = [name, this.guildId];

        await super.doTransaction(query, values).catch(error => Base.errorHandler(query, error));
        this.cache.guildName = name;
    }

    async deleteGuild(): Promise<void> {
        let query = `UPDATE guild SET deleted=true WHERE guild_id=${this.guildId};`;
        await super.doTransaction(query, undefined).catch(error => Base.errorHandler(query, error));

        if (this.cache !== undefined) this.cache.deleted = true;
    }

    async reactivateGuild(): Promise<void> {
        let query = `UPDATE guild SET deleted=false WHERE guild_id=${this.guildId};`;
        await super.doTransaction(query, undefined).catch(error => Base.errorHandler(query, error));

        this.cache.deleted = false;
    }
}
