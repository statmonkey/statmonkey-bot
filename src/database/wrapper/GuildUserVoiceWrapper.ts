import {Base} from "./Base";
import {Guild} from "../model/LinkedModel";

//Todo add data getting
export class GuildUserVoiceWrapper extends Base {
    constructor(guildId: string, cache: Guild) {
        super(guildId, cache);
    }

    addEntry(userId: string, guildId: string, channelId: string, activeStart: Date, activeEnd: Date): void {
        let query = 'INSERT INTO guild_user_voice(user_id, guild_id, voice_channel_id, active_start, active_end) VALUES ($1, $2, $3, $4, $5);';
        let values = [userId, guildId, channelId, activeStart, activeEnd];

        super.doTransaction(query, values).catch(error => Base.errorHandler(query, error));
    }
}
