import {RawMessageChannel} from "../model/RawModel";
import {Base} from './Base';
import {Guild, MessageChannel} from "../model/LinkedModel";

export class MessageChannelWrapper extends Base {
    readonly table: string;

    constructor(guildId: string, cache: Guild) {
        super(guildId, cache);
        this.table = "message_channel";
    }

    /*
    async getChannel(channelId: string): Promise<RawMessageChannel | undefined> {
        try{
            let result = await super.exists(this.table, "message_channel_id", channelId);
            if (result.exists) {
                return RawMessageChannel.buildDatabaseMapping(result.rows[0]);
            }
            else return undefined;
        }catch (e) {
            Base.errorHandler(undefined, e);
        }
    }
     */

    async getAllChannels(): Promise<RawMessageChannel[] | undefined> {
        try {
            let result = await super.exists(this.table, "guild_id", this.guildId);
            if (!result.exists) return undefined;
            let collectedRawMessage: RawMessageChannel[] = [];
            for (let i = 0; i !== result.rows.length; i++) {
                let dbCurrentChannel = result.rows[i];
                let rawMessageChannel = RawMessageChannel.buildDatabaseMapping(dbCurrentChannel);
                collectedRawMessage.push(rawMessageChannel);

                this.cache.messageChannel.set(rawMessageChannel.channelId, MessageChannel.convertToLinked(this.cache, rawMessageChannel));
            }
            return collectedRawMessage;
        } catch (e) {
            Base.errorHandler(undefined, e);
        }
    }

    async registerChannel(channelId: string, channelName: string, date: Date): Promise<void> {
        let query = 'INSERT INTO message_channel(message_channel_id, guild_id, channel_name, date_created) VALUES ($1, $2, $3, $4);';
        let values = [channelId, this.guildId, channelName, date];

        await super.doTransaction(query, values).catch(error => Base.errorHandler(query, error));
        this.cache.messageChannel.set(channelId, new MessageChannel(channelId, channelId, this.cache, date, undefined));
    }

    async registerChannels(channels: RawMessageChannel[]): Promise<void> {
        const client = await this.session.connect();
        try {
            let channelSearchQuery = "SELECT * FROM message_channel WHERE message_channel_id=$1";
            let channelAddQuery = "INSERT INTO message_channel(message_channel_id, guild_id, channel_name, date_created) VALUES ($1, $2, $3, $4)";
            for (let i = 0; i !== channels.length; i++) {
                let currentChannel = channels[i];

                //Check if the channel is registered
                let foundChannels = await client.query(channelSearchQuery, [currentChannel.channelId]);
                if (foundChannels.rows === undefined || foundChannels.rowCount === 0) {
                    await client.query(channelAddQuery, [currentChannel.channelId, currentChannel.guildId, currentChannel.channelName, currentChannel.dateCreated]);
                    this.cache.messageChannel.set(currentChannel.channelId, MessageChannel.convertToLinked(this.cache, currentChannel));
                }
                //Check if the channel is registered
            }
        } catch (e) {
            Base.errorHandler("Register Message Channel: ", e);
        } finally {
            client.release();
        }
    }

    async updateChannelName(channelId: string, channelName: string): Promise<void> {
        let query = 'UPDATE message_channel SET channel_name=$1 WHERE message_channel_id=$2;';
        let values = [channelName, channelId];

        await super.doTransaction(query, values).catch(error => Base.errorHandler(query, error));
        this.cache.messageChannel.get(channelId).channelName = channelName;
    }

    async deleteChannel(channelId: string): Promise<void> {
        let query = 'UPDATE message_channel SET date_deleted=$1 WHERE message_channel_id=$2;';
        let currentDate = new Date();

        let values = [currentDate, channelId];

        await super.doTransaction(query, values).catch(error => Base.errorHandler(query, error));
        this.cache.messageChannel.get(channelId).dateDeleted = currentDate;
    }
}
