'use strict';

import {Pool, QueryResult} from 'pg';
import {Guild} from "../model/LinkedModel";
import {Saving} from "../index";

export abstract class Base {
    readonly guildId: string;
    readonly guildName: string;
    readonly session: Pool;

    cache: Guild;

    protected constructor(guildId: string, cache: Guild) {
        this.guildId = guildId;
        this.session = Saving.pool;

        this.cache = cache;
    }

    static errorHandler(query: string | undefined, e: any): void {
        console.log(`Error from query: ${query}\nOutput of error: ${e}`);
    }

    doTransaction(sqlQuery: string, values: any [] = []): Promise<QueryResult> {
        return this.session.query(sqlQuery, values);
    }

    exists(table: string, row: string, value: any): Promise<ExistReturn> {
        let query = `SELECT * FROM ${table} where ${row}=${value}`;

        return this.doTransaction(query).then(result => {
            if (result.rows === undefined || result.rowCount === 0) {
                return {exists: false, rows: []};
            } else {
                return {exists: true, rows: result.rows}
            }
        }).catch(error => {
            throw error
        });
    }
}

class ExistReturn {
    exists: boolean;
    rows: any[];
}
