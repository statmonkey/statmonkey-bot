import {RawVoiceChannel} from "../model/RawModel";
import {Base} from "./Base";
import {Guild, VoiceChannel} from "../model/LinkedModel";

export class VoiceChannelWrapper extends Base {
    readonly table: string;

    constructor(guildId: string, cache: Guild) {
        super(guildId, cache);
        this.table = "voice_channel";
    }

    /*
    async getChannel(channelId: string): Promise<RawVoiceChannel> {
        try {
            let result = await super.exists(this.table, "voice_channel_id", channelId);
            if (result.exists) return RawVoiceChannel.buildDatabaseMapping(result.rows[0]);
            else return undefined;
        } catch (e) {
            Base.errorHandler("Error getting channel: ", e);
        }
    }
     */

    async getAllChannels(): Promise<RawVoiceChannel[]> {
        try {
            let result = await super.exists(this.table, "guild_id", this.guildId);
            if (!result.exists) return undefined;

            let rawVoiceChannels: RawVoiceChannel[] = [];

            for (let i = 0; i !== result.rows.length; i++) {
                let rawVoiceChannel = RawVoiceChannel.buildDatabaseMapping(result.rows[i]);
                this.cache.voiceChannel.set(rawVoiceChannel.channelId, VoiceChannel.convertToLinked(this.cache, rawVoiceChannel));
                rawVoiceChannels.push(rawVoiceChannel);
            }

            return rawVoiceChannels;
        } catch (e) {
            VoiceChannelWrapper.errorHandler(undefined, e);
        }
    }

    async registerChannel(channelId: string, channelName: string, date: Date): Promise<void> {
        let query = 'INSERT INTO voice_channel(voice_channel_id, guild_id, channel_name, date_created) VALUES ($1, $2, $3, $4);';
        let values = [channelId, this.guildId, channelName, date];

        await super.doTransaction(query, values).catch(error => Base.errorHandler(query, error));
        this.cache.voiceChannel.set(channelId, new VoiceChannel(channelId, channelName, this.cache, date, undefined));
    }

    async registerChannels(channels: RawVoiceChannel[]): Promise<void> {
        const client = await this.session.connect();

        try {
            let channelSearchQuery = "SELECT * FROM voice_channel WHERE voice_channel_id=$1";
            let channelAddQuery = "INSERT INTO voice_channel(voice_channel_id, guild_id, channel_name, date_created) VALUES ($1, $2, $3, $4)";
            for (let i = 0; i !== channels.length; i++) {
                let currentChannel = channels[i];

                //Check if the channel is registered
                let foundChannels = await client.query(channelSearchQuery, [currentChannel.channelId]);
                if (foundChannels.rows === undefined || foundChannels.rowCount === 0) {
                    await client.query(channelAddQuery, [currentChannel.channelId, currentChannel.guildId, currentChannel.channelName, currentChannel.dateCreated]);
                    this.cache.voiceChannel.set(currentChannel.channelId, new VoiceChannel(currentChannel.channelId, currentChannel.channelName, this.cache, currentChannel.dateCreated, undefined));
                }
                //Check if the channel is registered
            }
        } catch (e) {
            VoiceChannelWrapper.errorHandler("Register Voice Channels: ", e);
        } finally {
            client.release();
        }
    }

    async updateChannelName(channelId: string, channelName: string): Promise<void> {
        let query = 'UPDATE voice_channel SET channel_name=$1 WHERE voice_channel_id=$2;';
        let values = [channelName, channelId];

        await super.doTransaction(query, values).catch(error => Base.errorHandler(query, error));
        this.cache.voiceChannel.get(channelId).channelName = channelName;
    }

    async deleteChannel(channelId: string): Promise<void> {
        let query = 'UPDATE voice_channel SET date_deleted=$1 WHERE voice_channel_id=$2;';
        let currentDate = new Date();

        let values = [currentDate, channelId];

        await super.doTransaction(query, values).catch(error => Base.errorHandler(query, error));
        this.cache.voiceChannel.get(channelId).dateDeleted = currentDate;
    }
}
