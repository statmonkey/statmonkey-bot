# Statmonkey Bot
[Latest commit before Discord Hack Week Deadline](https://gitlab.com/statmonkey/statmonkey-bot/tree/3ca46746ffe958e8ec1ef078e47ab3275e9fbc72)

This bot is what is used to collect all the user data.  It includes a cache and command handling whilst also connecting to a database to log the minimal amount of data for the sake of user privacy.Extra steps has been added for the sake of user privacy, where they can 'turn on privacy' and their names would not show on the website.  We plan to eventually add a feature so the user isn't tracked completely.

(The bot is stable and can be used)